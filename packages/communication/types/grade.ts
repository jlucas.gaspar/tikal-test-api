import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@tikal/router';
import { GradeModel as Model } from '@tikal/types/services/grade';
import { Remove } from '@tikal/types/shared';

export enum GradeMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  Update = 'Update',
  GetByStudentId = 'GetByStudentId'
}

export namespace GradeInput {
  export type PingInternal = void;
  export type Create = Remove<Model.WithoutDatesAndId, 'average' | 'situation'>;
  export type Update = Partial<Remove<Model.WithoutDatesAndId, 'average' | 'situation'>> & Pick<Model.Complete, 'studentId'>;
  export type GetByStudentId = Pick<Model.Complete, 'studentId'>;
}

export namespace GradeOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type Update = Model.Complete;
  export type GetByStudentId = Model.Complete | undefined;
}

export interface GradeServiceRouter extends DefaultServiceRouter {
  [GradeMethods.PingInternal]: Handler<GradeInput.PingInternal, GradeOutput.PingInternal>;
  [GradeMethods.Create]: Handler<GradeInput.Create, GradeOutput.Create>;
  [GradeMethods.Update]: Handler<GradeInput.Update, GradeOutput.Update>;
  [GradeMethods.GetByStudentId]: Handler<GradeInput.GetByStudentId, GradeOutput.GetByStudentId>;
}