import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@tikal/router';
import { UserModel as Model } from '@tikal/types/services/user';

export enum UserMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  GetById = 'GetById',
  GetAllByRole = 'GetAllByRole'
}

export namespace UserInput {
  export type PingInternal = void;
  export type Create = Model.WithoutDatesAndId;
  export type GetById = Pick<Model.Complete, 'id'>;
  export type GetAllByRole = Pick<Model.Complete, 'role'>;
}

export namespace UserOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type GetById = Model.Complete | undefined;
  export type GetAllByRole = Model.Complete[];
}

export interface UserServiceRouter extends DefaultServiceRouter {
  [UserMethods.PingInternal]: Handler<UserInput.PingInternal, UserOutput.PingInternal>;
  [UserMethods.Create]: Handler<UserInput.Create, UserOutput.Create>;
  [UserMethods.GetById]: Handler<UserInput.GetById, UserOutput.GetById>;
  [UserMethods.GetAllByRole]: Handler<UserInput.GetAllByRole, UserOutput.GetAllByRole>;
}