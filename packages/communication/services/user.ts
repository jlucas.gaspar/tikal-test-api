import { ServicesNames } from '@tikal/types/shared';
import { createCommunicationRunner } from '../runner';
import {
  UserInput as Input,
  UserOutput as Output,
  UserMethods as Methods
} from '../types/user';

const run = createCommunicationRunner(ServicesNames.User);

type UserService = {
  pingInternal: (input: Input.PingInternal) => Promise<Output.PingInternal>;
  create: (input: Input.Create) => Promise<Output.Create>;
  getById: (input: Input.GetById) => Promise<Output.GetById>;
  getAllByRole: (input: Input.GetAllByRole) => Promise<Output.GetAllByRole>;
};

export const userService: UserService = {
  pingInternal: data => run(Methods.PingInternal, data),
  create: data => run(Methods.Create, data),
  getById: data => run(Methods.GetById, data),
  getAllByRole: data => run(Methods.GetAllByRole, data),
};