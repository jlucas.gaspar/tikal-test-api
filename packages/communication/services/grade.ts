import { ServicesNames } from '@tikal/types/shared';
import { createCommunicationRunner } from '../runner';
import {
  GradeInput as Input,
  GradeOutput as Output,
  GradeMethods as Methods
} from '../types/grade';

const run = createCommunicationRunner(ServicesNames.Grade);

type GradeService = {
  pingInternal: (input: Input.PingInternal) => Promise<Output.PingInternal>;
  create: (input: Input.Create) => Promise<Output.Create>;
  update: (input: Input.Update) => Promise<Output.Update>;
  getByStudentId: (input: Input.GetByStudentId) => Promise<Output.GetByStudentId>;
};

export const gradeService: GradeService = {
  pingInternal: data => run(Methods.PingInternal, data),
  create: data => run(Methods.Create, data),
  update: data => run(Methods.Update, data),
  getByStudentId: data => run(Methods.GetByStudentId, data),
};