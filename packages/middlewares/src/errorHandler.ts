import { ErrorRequestHandler } from 'express';

export const errorHandler: ErrorRequestHandler = (err, request, response, next) => {
  if (err.name && err.isAppError) {
    let errors;
    if (typeof err.message === 'string') errors = [err.message];
    else errors = err.message;

    return response.status(Number(err.statusCode)).json({ errors });
  }

  console.error(`
    ------ \n
    [${new Date().toLocaleString('pt-br')}] UNEXPECTED ERROR. \n
    Details: ${JSON.stringify(err)} \n
    ------
  `);
  return response.status(500).json({ errors: ['Intenal Server Error'] });
}