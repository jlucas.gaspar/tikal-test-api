import { RequestHandler } from 'express';

export const routeNotFound: RequestHandler = (request, response) => {
  const makeError = (errorString: string) => response.status(404).json({ errors: [errorString] });

  if (request.originalUrl.includes('/ping')) {
    if (request.method !== 'GET') {
      return makeError(`Wrong method ${request.method}. Only GET is allowed in "/ping" route.`);
    }
    return makeError(`You tried to reach a PING route without success. Please check if ${request.originalUrl} is correct.`);
  }

  return makeError(`${request.method} method in route ${request.originalUrl} not found.`);
}