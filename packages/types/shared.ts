type ServiceMap = {
  [ServName in ServicesNames]: {
    port: number;
  }
}

export enum ServicesNames {
  Api = 'api',
  User = 'user',
  Grade = 'grade',
}

export const Service: ServiceMap = {
  [ServicesNames.User]: { port: 5000 },
  [ServicesNames.Api]: { port: 5001 },
  [ServicesNames.Grade]: { port: 5002}
}

export type DatesAndId = {
  id: number;
  createdAt: Date;
  updatedAt?: Date | null;
  deletedAt?: Date | null;
}

export type Remove<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;