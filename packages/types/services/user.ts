import { DatesAndId, Remove } from '../shared';

export namespace UserModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;

  export type Complete = DatesAndId & {
    name: string;
    role: 'student' | 'teacher';
  }
}

export namespace UserHelpers {
  export type AuthJwtToken = { token: string; exp: number; }
  export type DecodedJwtToken = { exp: number; user: UserModel.Complete; }
}