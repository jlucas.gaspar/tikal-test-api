import { DatesAndId, Remove } from '../shared';

export namespace GradeModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;

  export type Complete = DatesAndId & {
    studentId: number;
    grade1: number;
    grade2: number;
    grade3: number;
    grade4: number;
    average: number;
    situation: GradeHelper.Situation;
  }
}

export namespace GradeHelper {
  // reprovado < 4 // recovery entre 4 a 6 // approved > 6
  export type Situation = 'approved' | 'disapproved' | 'recovery';
}