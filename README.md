### What you need to run this API
 - node (v14);
 - lerna (if you don't have it, just run `npm i -g lerna`);
 - run `lerna bootstrap` (it might take a while);

### To run all tests
 - ensure you've already run `lerna bootstrap` once;
 - run `npm run test:all` on the root folder (in the same level of lerna.json);

### To run tests of a specific service (api, user, or grade service)
 - ensure you've already run `lerna bootstrap` once;
 - Open the service folder (`cd services/service_folder_name`);
 - run `npm run test`;

## To open swagger docs in localhost
 - ensure you've already run `lerna bootstrap` once;
 - Open the service folder api (`cd services/api`);
 - run `npm start`;
 - open http://localhost:5001/api/public/api-docs on your browser;