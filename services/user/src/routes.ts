import {
  UserServiceRouter as ServiceRouter,
  UserInput as Input,
  UserMethods as Methods
} from '@tikal/communication/types/user';
import { routerHandler, Router } from '@tikal/router';
import { Joi } from '@tikal/validator';
import { serviceName } from './utils/serviceInfo';
import { url } from './utils/url';
import * as core from './core';

const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  },

  [Methods.Create]: {
    handler: core.create,
    schema: Joi.object<Input.Create>().keys({
      name: Joi.string().required(),
      role: Joi.string().required().valid('student', 'teacher')
    })
  },

  [Methods.GetAllByRole]: {
    handler: core.getAllByRole,
    schema: Joi.object<Input.GetAllByRole>().keys({
      role: Joi.string().required().valid('student', 'teacher')
    })
  },

  [Methods.GetById]: {
    handler: core.getById,
    schema: Joi.object<Input.GetById>().keys({
      id: Joi.number().required()
    })
  },
}));

export const routes = router;