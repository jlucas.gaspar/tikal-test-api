import { UserInput as Input, UserOutput as Output } from '@tikal/communication/types/user';
import { repository } from '../repository';

type Core = (params: Input.GetById) => Promise<Output.GetById>;

export const getById: Core = async ({ id }) => {
  return await repository.findById(id);
}