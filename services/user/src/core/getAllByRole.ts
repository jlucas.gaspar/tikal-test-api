import { UserInput as Input, UserOutput as Output } from '@tikal/communication/types/user';
import { repository } from '../repository';

type Core = (params: Input.GetAllByRole) => Promise<Output.GetAllByRole>;

export const getAllByRole: Core = async ({ role }) => {
  return await repository.findAllByRole({ role });
}