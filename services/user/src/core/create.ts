import { UserInput as Input, UserOutput as Output } from '@tikal/communication/types/user';
import { repository } from '../repository';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async ({ name, role }) => {
  const user = await repository.insert({ name, role });
  return user;
}