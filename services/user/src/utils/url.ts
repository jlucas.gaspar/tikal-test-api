import { Url } from '@tikal/router';
import { serviceName } from './serviceInfo';

export const url = (url: Url) => `/${serviceName}${url}`;