import knex from 'knex';
import { UserModel as Model } from '@tikal/types/services/user';
import { BadRequestError } from '@tikal/error';
import knexfile, { TABLE_NAME } from '../knexfile';

const getTable = () => {
  try {
    const database = knex(knexfile);
    return database<Model.Complete>(TABLE_NAME);
  } catch (err) {
    throw new Error(`Error on opening conn with PostgreSQL. Error details: ${err}`);
  }
}

export const repository = {
  insert: async (params: Model.WithoutDatesAndId) => {
    try {
      const data = await getTable().insert(params).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findById: async (id: number) => {
    try {
      return await getTable().where('id', id).first();
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findAllByRole: async ({ role }: Pick<Model.Complete, 'role'>) => {
    try {
      return await getTable().where('role', role).whereNull('deletedAt').select('*');
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }
}