import { resetDatabase } from './mocks';
import { create } from '../core/create';
import { getAllByRole as sut } from '../core/getAllByRole';

describe('user service :: getAllByRole', () => {
  beforeEach(() => resetDatabase());

  it('should get all users by role correctly', async () => {
    await create({ name: 'João Silva', role: 'teacher' });
    await create({ name: 'João Silva', role: 'student' });
    const students = await sut({ role: 'student' });
    const teachers = await sut({ role: 'teacher' });
    expect(students.length).toEqual(1);
    expect(teachers.length).toEqual(1);
  });
});