import { resetDatabase } from './mocks';
import { create } from '../core/create';
import { getById as sut } from '../core/getById';

describe('user service :: getById', () => {
  beforeEach(() => resetDatabase());

  it('should get an user by id correctly', async () => {
    const teacher = await create({ name: 'João Silva', role: 'teacher' });
    const foundTeacher = await sut({ id: teacher.id });
    expect(foundTeacher).toBeTruthy();
  });
});