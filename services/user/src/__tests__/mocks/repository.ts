import { UserModel as Model } from '@tikal/types/services/user';
import { repository as Repository } from '../../repository';

let database: Model.Complete[] = [];

export const resetDatabase = () => database = [];

export const repository: typeof Repository = {
  insert: async (params: Model.WithoutDatesAndId) => {
    const data = {
      ...params,
      id: database.length + 1,
      createdAt: new Date()
    }

    database.push(data);

    return data;
  },

  findById: async (id: number) => {
    return database.find(_ => _.id === id);
  },

  findAllByRole: async ({ role }: Pick<Model.Complete, 'role'>) => {
    return database.filter(_ => _.role === role);
  }
}