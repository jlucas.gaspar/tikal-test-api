import { resetDatabase } from './mocks';
import { create as sut } from '../core/create';

describe('user service :: create', () => {
  beforeEach(() => resetDatabase());

  it('should create an user correctly', async () => {
    const result = await sut({ name: 'João Silva', role: 'teacher' });
    expect(result.id).toBeTruthy();
  });
});