const { resolve } = require('path');

const root = resolve(__dirname);
const packagesFolder = resolve(__dirname, '..', '..', 'packages');

module.exports = {
  rootDir: root,
  testMatch: ['<rootDir>/src/__tests__/**/*.test.ts'],
  collectCoverage: true,
  coverageThreshold: {
    global: {
      lines: 90,
      statements: 90
    }
  },
  moduleFileExtensions: ['ts', 'js'],
  collectCoverageFrom: [
    '<rootDir>/src/controllers/**/*.ts',
    '!<rootDir>/src/controllers/**/index.ts'
  ],
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  clearMocks: true,
  preset: 'ts-jest',
  moduleNameMapper: {
    '@tikal/(.*)': `${packagesFolder}/$1`
  }
}