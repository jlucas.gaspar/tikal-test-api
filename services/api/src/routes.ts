import swaggerUi from 'swagger-ui-express';
import { Router } from 'express';
import { swaggerJson } from '../swagger-docs';
import { controllerWrapper } from './utils/controllerWrapper';
import { serviceName } from './utils/serviceInfo';
import * as grade from './controllers/grade';
import * as user from './controllers/user';

const route = Router();

if (process.env.APP_MODE === 'local') {
  route.use('/public/api-docs', swaggerUi.serve);
  route.get('/public/api-docs', swaggerUi.setup(swaggerJson));
}

route.get('/public/ping', (request, response) => {
  return response.json(`PING on service ${serviceName}`);
});

route.post('/public/user', controllerWrapper(user.createUserController));

route.get('/grades/all', controllerWrapper(grade.getAllGradesController));
route.post('/grades/student', controllerWrapper(grade.createGradeController));
route.put('/grades/student/:studentId', controllerWrapper(grade.updateGradeController));
route.get('/grades/student/:studentId', controllerWrapper(grade.getGradeByStudentIdController));

export const routes = route;