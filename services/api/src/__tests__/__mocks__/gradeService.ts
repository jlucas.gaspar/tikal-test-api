import { gradeService as GradeService } from '@tikal/communication/services/grade';

export const gradeService: typeof GradeService = {
  create: async ({ grade1, grade2, grade3, grade4, studentId }) => await ({
    id: Math.round(Math.random() * 10000),
    studentId,
    grade1,
    grade2,
    grade3,
    grade4,
    situation: 'approved',
    average: 10,
    createdAt: new Date()
  }),

  getByStudentId: async ({ studentId }) => await ({
    id: Math.round(Math.random() * 10000),
    studentId,
    grade1: 10,
    grade2: 10,
    grade3: 10,
    grade4: 10,
    situation: 'approved',
    average: 10,
    createdAt: new Date()
  }),

  update: async ({ studentId, grade1, grade2, grade3, grade4 }) => await ({
    id: Math.round(Math.random() * 10000),
    studentId,
    grade1: grade1 !== undefined ? grade1 : 10,
    grade2: grade2 !== undefined ? grade2 : 10,
    grade3: grade3 !== undefined ? grade3 : 10,
    grade4: grade4 !== undefined ? grade4 : 10,
    situation: 'approved',
    average: 10,
    createdAt: new Date()
  }),

  pingInternal: async () => await 'ping'
}