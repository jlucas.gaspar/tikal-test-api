import { userService as UserService } from '@tikal/communication/services/user';

export const userService: typeof UserService = {
  create: async ({ name, role }) => await ({
    id: Math.round(Math.random() * 10000),
    name,
    role,
    createdAt: new Date()
  }),

  getAllByRole: async ({ role }) => await [
    {
      id: Math.round(Math.random() * 10000),
      name: 'any_name',
      role,
      createdAt: new Date()
    },
    {
      id: Math.round(Math.random() * 10000),
      name: 'any_name',
      role,
      createdAt: new Date()
    },
  ],

  getById: async ({ id }) => await ({
    id,
    name: 'any_name',
    role: 'teacher',
    createdAt: new Date()
  }),

  pingInternal: async () => await 'ping'
}