import '../__mocks__';
import { userService } from '../__mocks__/userService';
import { BadRequestError, InvalidParamError } from '@tikal/error';
import { UserModel } from '@tikal/types/services/user';
import { updateGradeController as sut } from '../../controllers/grade';

describe('api service :: grade :: updateGradeController', () => {
  it('should throw if request body is not correct', async () => {
    const promise = sut({
      body: {
        grade1: 50,
      },
      params: {
        studentId: 5
      },
      query: {},
      user: {} as any
    });

    expect(promise).rejects.toBeInstanceOf(InvalidParamError);
  });

  it('should throw if request user is not a teacher', async () => {
    const promise = sut({
      body: {
        grade1: 10,
        grade4: 8
      },
      params: {
        studentId: 1
      },
      query: {},
      user: {
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the provided studentId does not exists', async () => {
    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(undefined));

    const promise = sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
      },
      params: {
        studentId: 500
      },
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the found studentId role is not "student"', async () => {
    let user = await userService.getById({ id: 500 });
    if (user) {
      user.role = 'teacher';
    }

    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(user));

    const promise = sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
      },
      params: {
        studentId: 500
      },
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should return the updated grade correctly', async () => {
    const user = await userService.getById({ id: 500 });
    if (user) {
      jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve({ ...user, role: 'student' }));
    }

    const result = await sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
      },
      params: {
        studentId: 500
      },
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(result).toBeTruthy();
  });
});