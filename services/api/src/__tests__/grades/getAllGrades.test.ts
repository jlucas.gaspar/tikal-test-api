import '../__mocks__';
import { BadRequestError } from '@tikal/error';
import { UserModel } from '@tikal/types/services/user';
import { getAllGradesController as sut } from '../../controllers/grade';

describe('api service :: grade :: getAllGradesController', () => {
  it('should throw if request user is not a teacher', async () => {
    const promise = sut({
      body: {},
      params: {},
      query: {},
      user: {
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should return an array with students and their grades', async () => {
    const result = await sut({
      body: {},
      params: {},
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(result.length).toBeTruthy();
  });
});