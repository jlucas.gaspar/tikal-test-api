import '../__mocks__';
import { userService } from '../__mocks__/userService';
import { BadRequestError, InvalidParamError } from '@tikal/error';
import { UserModel } from '@tikal/types/services/user';
import { createGradeController as sut } from '../../controllers/grade';

describe('api service :: grade :: createGradeController', () => {
  it('should throw if request body is not correct', async () => {
    const promise = sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: undefined as any,
        studentId: 5
      },
      params: {},
      query: {},
      user: {} as any
    });

    expect(promise).rejects.toBeInstanceOf(InvalidParamError);
  });

  it('should throw if request user is not a teacher', async () => {
    const promise = sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
        studentId: 1
      },
      params: {},
      query: {},
      user: {
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the provided studentId does not exists', async () => {
    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(undefined));

    const promise = sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
        studentId: 500
      },
      params: {},
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the found studentId role is not "student"', async () => {
    let user = await userService.getById({ id: 500 });
    if (user) {
      user.role = 'teacher';
    }

    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(user));

    const promise = sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
        studentId: 500
      },
      params: {},
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should return the created grade correctly', async () => {
    let user = await userService.getById({ id: 500 });
    if (user) {
      user.role = 'student';
    }

    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(user));

    const result = await sut({
      body: {
        grade1: 10,
        grade2: 10,
        grade3: 9,
        grade4: 8,
        studentId: 500
      },
      params: {},
      query: {},
      user: {
        role: 'teacher'
      } as UserModel.Complete
    });

    expect(result).toBeTruthy();
  });
});