import '../__mocks__';
import { BadRequestError } from '@tikal/error';
import { UserModel } from '@tikal/types/services/user';
import { getGradeByStudentIdController as sut } from '../../controllers/grade';
import { userService } from '../__mocks__/userService';
import { gradeService } from '../__mocks__/gradeService';

describe('api service :: grade :: getGradeByStudentIdController', () => {
  it('should throw if request user role is a student, but the studentId in params does not match with the request user id', async () => {
    const promise = sut({
      body: {},
      params: { studentId: 10 },
      query: {},
      user: {
        id: 50,
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if studentId params is not found', async () => {
    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(undefined));

    const promise = sut({
      body: {},
      params: { studentId: 10 },
      query: {},
      user: {
        id: 10,
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the found studentId is not a student', async () => {
    const userFound = await userService.getById({ id: 100 });
    if (userFound) {
      userFound.role = 'teacher'
    }

    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(userFound));

    const promise = sut({
      body: {},
      params: { studentId: 10 },
      query: {},
      user: {
        id: 10,
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the grade by studentId is not found', async () => {
    const userFound = await userService.getById({ id: 100 });
    if (userFound) {
      userFound.role = 'student'
    }

    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(userFound));
    jest.spyOn(gradeService, 'getByStudentId').mockReturnValueOnce(Promise.resolve(undefined));

    const promise = sut({
      body: {},
      params: { studentId: 10 },
      query: {},
      user: {
        id: 10,
        role: 'student'
      } as UserModel.Complete
    });

    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should return the grade correctly', async () => {
    const userFound = await userService.getById({ id: 100 });
    if (userFound) {
      userFound.role = 'student'
    }

    jest.spyOn(userService, 'getById').mockReturnValueOnce(Promise.resolve(userFound));

    const result = await sut({
      body: {},
      params: { studentId: 10 },
      query: {},
      user: {
        id: 10,
        role: 'student'
      } as UserModel.Complete
    });

    expect(result).toBeTruthy();
  });
});