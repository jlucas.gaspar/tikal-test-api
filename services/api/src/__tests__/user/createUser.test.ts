import '../__mocks__';
import { InvalidParamError } from '@tikal/error';
import { createUserController as sut } from '../../controllers/user';

describe('api service :: user :: createUserController', () => {
  it('should throw if request body is not correct', async () => {
    const promise = sut({
      body: {
        name: '',
        role: '' as any
      },
      params: {},
      query: {},
      user: {} as any
    });

    expect(promise).rejects.toBeInstanceOf(InvalidParamError);
  });

  it('should create an user correctly', async () => {
    const result = await sut({
      body: {
        name: 'Joao Santos',
        role: 'student'
      },
      params: {},
      query: {},
      user: {} as any
    });

    expect(result).toBeTruthy();
  });
});