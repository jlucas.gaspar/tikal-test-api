import { Request, Response, NextFunction } from 'express';
import { BadRequestError, UnauthorizedError } from '@tikal/error';
import { userService } from '@tikal/communication/services/user';

export const getRequestUser = async (request: Request, response: Response, next: NextFunction) => {
  if (request.originalUrl.includes('/public')) {
    return next();
  }

  const userId = request.headers['x-user-id'] as string;

  if (!userId) {
    throw new UnauthorizedError('Please provide your "x-user-id" in request headers to access this route');
  }

  const user = await userService.getById({ id: Number(userId) });

  if (!user) {
    throw new BadRequestError('Request user not found. Ensure your "x-user-id" in headers is correct.');
  }

  request.user = user;

  return next();
}