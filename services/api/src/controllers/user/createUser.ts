import { userService } from '@tikal/communication/services/user';
import { UserInput } from '@tikal/communication/types/user';
import { UserModel } from '@tikal/types/services/user';
import { Joi } from '@tikal/validator';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = UserInput.Create;
type Params = {};
type Query = {};
type Return = UserModel.Complete;
type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const createUserController: Controller = async (request) => {
  validateSchema<Body>(request.body, createUserSchema);
  const { name, role } = request.body;
  return await userService.create({ name, role });
}

export const createUserSchema = Joi.object<Body>().keys({
  name: Joi.string().required(),
  role: Joi.string().required().valid('teacher', 'student')
});