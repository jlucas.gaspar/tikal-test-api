import { GradeInput, GradeOutput } from '@tikal/communication/types/grade';
import { gradeService } from '@tikal/communication/services/grade';
import { userService } from '@tikal/communication/services/user';
import { BadRequestError } from '@tikal/error';
import { Joi } from '@tikal/validator';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = GradeInput.Create;
type Params = {};
type Query = {};
type Return = GradeOutput.Create;
type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const createGradeController: Controller = async (request) => {
  validateSchema<Body>(request.body, createGradeSchema);

  if (request.user.role !== 'teacher') {
    throw new BadRequestError('You can not create grades to other students.');
  }

  const studentExists = await userService.getById({ id: request.body.studentId });
  if (!studentExists) {
    throw new BadRequestError('Student not found.');
  }

  if (studentExists.role !== 'student') {
    throw new BadRequestError('You can not add grades of a teacher.');
  }

  return await gradeService.create(request.body);
}

export const createGradeSchema = Joi.object<Body>().keys({
  studentId: Joi.number().required(),
  grade1: Joi.number().required().max(10).min(0),
  grade2: Joi.number().required().max(10).min(0),
  grade3: Joi.number().required().max(10).min(0),
  grade4: Joi.number().required().max(10).min(0)
});