import { gradeService } from '@tikal/communication/services/grade';
import { userService } from '@tikal/communication/services/user';
import { GradeModel } from '@tikal/types/services/grade';
import { UserModel } from '@tikal/types/services/user';
import { BadRequestError } from '@tikal/error';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = {};
type Query = {};
type Return = Array<{ student: UserModel.Complete; grades: GradeModel.Complete | undefined; }>
type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllGradesController: Controller = async (request) => {
  if (request.user.role !== 'teacher') {
    throw new BadRequestError('Your role can not see all grades of the students.');
  }

  const finalArray: Return = [];

  const students = await userService.getAllByRole({ role: 'student' });

  for (const student of students) {
    const grades = await gradeService.getByStudentId({ studentId: student.id });
    finalArray.push({ student, grades });
  }

  return finalArray;
}