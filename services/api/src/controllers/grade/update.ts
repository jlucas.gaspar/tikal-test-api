import { GradeInput, GradeOutput } from '@tikal/communication/types/grade';
import { gradeService } from '@tikal/communication/services/grade';
import { userService } from '@tikal/communication/services/user';
import { BadRequestError } from '@tikal/error';
import { Remove } from '@tikal/types/shared';
import { Joi } from '@tikal/validator';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = Remove<GradeInput.Update, 'studentId'>;
type Params = Pick<GradeInput.Update, 'studentId'>;
type Query = {};
type Return = GradeOutput.Update;
type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const updateGradeController: Controller = async (request) => {
  validateSchema<Body>(request.body, updateGradeSchema);

  if (request.user.role !== 'teacher') {
    throw new BadRequestError('You can not update grades to other students.');
  }

  const studentExists = await userService.getById({ id: request.params.studentId });
  if (!studentExists) {
    throw new BadRequestError('Student not found.');
  }

  if (studentExists.role !== 'student') {
    throw new BadRequestError('You can not update grades of a teacher.');
  }

  return await gradeService.update({
    ...request.body,
    studentId: request.params.studentId
  });
}

export const updateGradeSchema = Joi.object<Body>().keys({
  grade1: Joi.number().optional().max(10).min(0),
  grade2: Joi.number().optional().max(10).min(0),
  grade3: Joi.number().optional().max(10).min(0),
  grade4: Joi.number().optional().max(10).min(0)
});