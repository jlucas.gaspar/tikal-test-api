import { gradeService } from '@tikal/communication/services/grade';
import { GradeModel } from '@tikal/types/services/grade';
import { BadRequestError } from '@tikal/error';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { userService } from '@tikal/communication/services/user';

type Body = {};
type Params = Pick<GradeModel.Complete, 'studentId'>;
type Query = {};
type Return = GradeModel.Complete;
type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getGradeByStudentIdController: Controller = async (request) => {
  if (request.user.role === 'student' && request.user.id !== request.params.studentId) {
    throw new BadRequestError('You can not access other students grades.');
  }

  const user = await userService.getById({ id: request.params.studentId });
  if (!user) {
    throw new BadRequestError('Student not found.');
  }

  if (user.role !== 'student') {
    throw new BadRequestError('This provided student ID does not belongs an user that role is "student".');
  }

  const grade = await gradeService.getByStudentId({ studentId: request.params.studentId });

  if (!grade) {
    throw new BadRequestError('Grades not found for this student ID');
  }

  return grade;
}