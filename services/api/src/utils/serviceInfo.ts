import { Service, ServicesNames } from '@tikal/types/shared';

const service = ServicesNames.Api;

export const serviceName = service;
export const servicePort = Service[serviceName].port;