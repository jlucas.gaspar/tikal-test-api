import { makePath } from '../utils';

export const pingPaths = {
  '/public/ping': makePath('health-check', {
    get: {
      summary: 'Check API status health',
      description: 'Ensure the API is healthy',
      response: {
        description: 'Application is OK'
      }
    }
  })
}