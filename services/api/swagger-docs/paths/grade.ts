import { createGradeSchema, updateGradeSchema } from '../../src/controllers/grade';
import { makePath, makeResponse } from '../utils';

export const gradePaths = {
  '/grades/student': makePath('grades', {
    post: {
      summary: 'Create grades of a student',
      description: 'Create grades of a student',
      request: {
        params: [
          {
            in: 'header',
            name: 'x-user-id',
            required: true,
            type: 'integer',
            description: 'Your userId'
          }
        ],
        body: {
          description: 'Create grade request body',
          schema: createGradeSchema
        },
      },
      response: makeResponse('Grade created', '#/definitions/Grade')
    }
  }),

  '/grades/student/{studentId}': makePath('grades', {
    put: {
      summary: 'Update grades of a student',
      description: 'Update grades of a student',
      request: {
        params: [
          {
            in: 'header',
            name: 'x-user-id',
            required: true,
            type: 'integer',
            description: 'Your userId'
          },
          {
            in: 'path',
            name: 'studentId',
            required: true,
            type: 'integer',
            description: 'The studentId that will be updated'
          }
        ],
        body: {
          description: 'Update grade request body',
          schema: updateGradeSchema
        },
      },
      response: makeResponse('Grade updated', '#/definitions/Grade')
    },
    get: {
      summary: 'Get all grades of one student',
      description: 'Get all grades of one student',
      request: {
        params: [
          {
            in: 'header',
            name: 'x-user-id',
            required: true,
            type: 'integer',
            description: 'Your userId'
          },
          {
            in: 'path',
            name: 'studentId',
            required: true,
            type: 'integer',
            description: 'The studentId that you want to access the grades'
          }
        ],
      },
      response: makeResponse('Found all grades of this students', '#/definitions/Grade')
    }
  }),

  '/grades/all': makePath('grades', {
    get: {
      summary: 'Get all grades of all student',
      description: 'Get all grades of all student',
      request: {
        params: [
          {
            in: 'header',
            name: 'x-user-id',
            required: true,
            type: 'integer',
            description: 'Your userId'
          }
        ]
      },
      response: makeResponse('Found all grades of all students', {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            student: { $ref: '#/definitions/User' },
            grades: { $ref: '#/definitions/Grade' },
          }
        }
      })
    }
  }),
}