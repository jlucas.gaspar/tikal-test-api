import { createUserSchema } from '../../src/controllers/user';
import { makePath, makeResponse } from '../utils';

export const userPaths = {
  '/public/user': makePath('users', {
    post: {
      summary: 'Create user',
      description: 'Create user',
      request: {
        body: {
          description: 'Create user request body',
          schema: createUserSchema
        }
      },
      response: makeResponse('User created', '#/definitions/User')
    }
  })
}