import { pingPaths } from './ping';
import { userPaths } from './user';
import { gradePaths } from './grade';

export const paths = {
  ...pingPaths,
  ...userPaths,
  ...gradePaths,
}