import { definitions } from './definitions';
import { paths } from './paths';

export const swaggerJson = {
  swagger: "2.0",
  info: {
    title: 'Tikal test',
    description: 'Documentation for API made in Tikal test',
    version: '1.0.0',
  },
  produces: [
    "application/json"
  ],
  host: 'tm5q36o2ec.execute-api.us-east-1.amazonaws.com',
  basePath: '/prod/api',
  schemes: ['https'],
  definitions,
  paths,
  tags: [
    { name: 'health-check', description: 'Ping route' },
    { name: 'users', description: 'Everything related to users' },
    { name: 'grades', description: 'Everything related to grades ("x-user-id" in headers is necessary)' }
  ]
}