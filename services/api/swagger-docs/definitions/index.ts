import { Grade } from './Grade';
import { User } from './User';
import { BadRequestError } from './BadRequestError';
import { SchemaValidationError } from './SchemaValidationError';

export const definitions = {
  Grade,
  User,
  BadRequestError,
  SchemaValidationError
}