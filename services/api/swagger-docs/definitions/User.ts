import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';
import { UserModel } from '@tikal/types/services/user';
import { Remove } from '@tikal/types/shared';

type Model = Remove<UserModel.Complete, 'createdAt' | 'deletedAt' | 'updatedAt'>;

export const User = joiToSwagger(Joi.object<Model>().keys({
  id: Joi.number().required()
    .description('user ID')
    .example(1),
  name: Joi.string().required()
    .description('user name')
    .example('João da Silva'),
  role: Joi.string().required()
    .valid('teacher', 'student')
    .description('user role')
    .example('teacher | student')
})).swagger;