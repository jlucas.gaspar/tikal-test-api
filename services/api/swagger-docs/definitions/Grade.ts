import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';
import { GradeModel } from '@tikal/types/services/grade';
import { Remove } from '@tikal/types/shared';

type Model = Remove<GradeModel.Complete, 'createdAt' | 'deletedAt' | 'updatedAt'>;

export const Grade = joiToSwagger(Joi.object<Model>().keys({
  id: Joi.number().required()
    .description('grade ID')
    .example(1),
  studentId: Joi.number().required()
    .description('student ID')
    .example(1),
  grade1: Joi.number().required()
    .min(0)
    .max(10)
    .description('student grade 1')
    .example(5),
  grade2: Joi.number().required()
    .min(0)
    .max(10)
    .description('student grade 2')
    .example(6.8),
  grade3: Joi.number().required()
    .min(0)
    .max(10)
    .description('student grade 3')
    .example(7.3),
  grade4: Joi.number().required()
    .min(0)
    .max(10)
    .description('student grade 4')
    .example(8.5),
  average: Joi.number().required()
    .min(0)
    .max(10)
    .description('student grade average')
    .example(7.7),
  situation: Joi.string().required()
    .valid('approved', 'disapproved', 'recovery')
    .description('student situation according to his/her average')
    .example('approved'),
})).swagger;