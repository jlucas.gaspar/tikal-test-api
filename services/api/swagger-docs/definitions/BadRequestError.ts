export const BadRequestError = {
  description: 'Bad Request',
  type: 'object',
  properties: {
    errors: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  },
  example: {
    errors: [
      'You can not access this data'
    ]
  }
}