import {
  GradeServiceRouter as ServiceRouter,
  GradeInput as Input,
  GradeMethods as Methods
} from '@tikal/communication/types/grade';
import { routerHandler, Router } from '@tikal/router';
import { Joi } from '@tikal/validator';
import { serviceName } from './utils/serviceInfo';
import { url } from './utils/url';
import * as core from './core';

const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  },

  [Methods.Create]: {
    handler: core.create,
    schema: Joi.object<Input.Create>().keys({
      studentId: Joi.number().required(),
      grade1: Joi.number().required().max(10).min(0),
      grade2: Joi.number().required().max(10).min(0),
      grade3: Joi.number().required().max(10).min(0),
      grade4: Joi.number().required().max(10).min(0),
    })
  },

  [Methods.Update]: {
    handler: core.update,
    schema: Joi.object<Input.Update>().keys({
      studentId: Joi.number().required(),
      grade1: Joi.number().optional().max(10).min(0),
      grade2: Joi.number().optional().max(10).min(0),
      grade3: Joi.number().optional().max(10).min(0),
      grade4: Joi.number().optional().max(10).min(0)
    })
  },

  [Methods.GetByStudentId]: {
    handler: core.getByStudentId,
    schema: Joi.object<Input.GetByStudentId>().keys({
      studentId: Joi.number().required()
    })
  },
}));

export const routes = router;