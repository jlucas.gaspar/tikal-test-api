import { GradeInput as Input, GradeOutput as Output } from '@tikal/communication/types/grade';
import { BadRequestError } from '@tikal/error';
import { GradeHelper } from '@tikal/types/services/grade';
import { repository } from '../repository';

type Core = (params: Input.Update) => Promise<Output.Update>;

export const update: Core = async ({ studentId, ...updateGradesParams }) => {
  if (Object.keys(updateGradesParams).length === 0) {
    throw new BadRequestError('You must at least provide one grade to update.');
  }

  const grades = await repository.findByStudentId(studentId);

  if (!grades) {
    throw new BadRequestError('Grades not found for this student ID');
  }

  const newGrades = {
    grade1: updateGradesParams.grade1 === undefined ? grades.grade1 : updateGradesParams.grade1,
    grade2: updateGradesParams.grade2 === undefined ? grades.grade2 : updateGradesParams.grade2,
    grade3: updateGradesParams.grade3 === undefined ? grades.grade3 : updateGradesParams.grade3,
    grade4: updateGradesParams.grade4 === undefined ? grades.grade4 : updateGradesParams.grade4,
  }

  const newAverage = (newGrades.grade1 + newGrades.grade2 + newGrades.grade3 + newGrades.grade4) / 4;

  let newSituation: GradeHelper.Situation = '' as GradeHelper.Situation;

  if (newAverage < 4) newSituation = 'disapproved';
  else if (newAverage < 6) newSituation = 'recovery';
  else newSituation = 'approved';

  return await repository.update(studentId, {
    ...newGrades,
    average: newAverage,
    situation: newSituation
  });
}