import { GradeInput as Input, GradeOutput as Output } from '@tikal/communication/types/grade';
import { repository } from '../repository';

type Core = (params: Input.GetByStudentId) => Promise<Output.GetByStudentId>;

export const getByStudentId: Core = async ({ studentId }) => {
  return await repository.findByStudentId(studentId);
}