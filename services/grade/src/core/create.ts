import { GradeInput as Input, GradeOutput as Output } from '@tikal/communication/types/grade';
import { GradeHelper } from '@tikal/types/services/grade';
import { repository } from '../repository';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async ({ grade1, grade2, grade3, grade4, studentId }) => {
  const average = (grade1 + grade2 + grade3 + grade4) / 4;

  let situation: GradeHelper.Situation = '' as GradeHelper.Situation;

  if (average < 4) situation = 'disapproved';
  else if (average < 6) situation = 'recovery';
  else situation = 'approved';

  return await repository.insert({
    studentId,
    grade1,
    grade2,
    grade3,
    grade4,
    average,
    situation
  });
}