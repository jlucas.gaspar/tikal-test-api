import knex from 'knex';
import { GradeModel as Model } from '@tikal/types/services/grade';
import { BadRequestError } from '@tikal/error';
import { Remove } from '@tikal/types/shared';
import knexfile, { TABLE_NAME } from '../knexfile';

const getTable = () => {
  try {
    const database = knex(knexfile);
    return database<Model.Complete>(TABLE_NAME);
  } catch (err) {
    throw new Error(`Error on opening conn with PostgreSQL. Error details: ${err}`);
  }
}

export const repository = {
  insert: async (params: Model.WithoutDatesAndId) => {
    try {
      const data = await getTable().insert(params).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  update: async (studentId: number, params: Partial<Remove<Model.WithoutDatesAndId, 'studentId'>>) => {
    try {
      const data = await getTable().where('studentId', studentId).update({ ...params, updatedAt: new Date() }).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findByStudentId: async (studentId: number) => {
    try {
      return await getTable().where('studentId', studentId).first();
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  }
}