import { BadRequestError } from '@tikal/error';
import { resetDatabase } from './mocks';
import { create } from '../core/create';
import { update as sut } from '../core/update';

describe('grade service :: update', () => {
  beforeEach(() => resetDatabase());

  it('should throw if any grade is provided', async () => {
    const promise = sut({ studentId: 10 });
    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the studentId provided does not exists', async () => {
    const promise = sut({ studentId: 10, grade1: 9 });
    expect(promise).rejects.toBeInstanceOf(BadRequestError);
  });

  it('should throw if the studentId provided does not exists', async () => {
    await create({
      studentId: 10,
      grade1: 9,
      grade2: 8,
      grade3: 8,
      grade4: 9
    });
    const result1 = await sut({
      studentId: 10,
      grade2: 9,
      grade3: 9
    });
    expect(result1.average).toEqual(9);

    await create({
      studentId: 5,
      grade1: 5,
      grade2: 4,
      grade3: 4,
      grade4: 5
    });
    const result2 = await sut({
      studentId: 5,
      grade2: 5,
      grade3: 5
    });
    expect(result2.average).toEqual(5);

    await create({
      studentId: 20,
      grade1: 2,
      grade2: 3,
      grade3: 3,
      grade4: 2
    });
    const result3 = await sut({
      studentId: 20,
      grade2: 4,
      grade3: 4
    });
    expect(result3.average).toEqual(3);
  });
});