import e from '@tikal/middlewares/node_modules/@types/express';
import { GradeModel as Model } from '@tikal/types/services/grade';
import { repository as Repository } from '../../repository';

let database: Model.Complete[] = [];

export const resetDatabase = () => database = [];

export const repository: typeof Repository = {
  insert: async (params: Model.WithoutDatesAndId) => {
    const data = {
      ...params,
      id: database.length + 1,
      createdAt: new Date()
    }

    database.push(data);

    return data;
  },

  update: async (studentId, params) => {
    let foundData = {} as Model.Complete;
    
    database = database.map(grade => {
      if (grade.studentId !== studentId) return grade;

      foundData = { ...grade, ...params }
      return foundData;
    });

    return foundData;
  },

  findByStudentId: async (studentId) => {
    return database.find(_ => _.studentId === studentId);
  }
}