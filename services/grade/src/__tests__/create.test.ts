import { resetDatabase } from './mocks';
import { create as sut } from '../core/create';

describe('grade service :: create', () => {
  beforeEach(() => resetDatabase());

  it('should create a grade correctly', async () => {
    const result1 = await sut({
      studentId: 1,
      grade1: 5,
      grade2: 6,
      grade3: 7,
      grade4: 4
    });
    expect(result1.id).toBeTruthy();
    expect(result1.average).toEqual(5.5);
    expect(result1.situation).toEqual('recovery');

    const result2 = await sut({
      studentId: 1,
      grade1: 8,
      grade2: 8,
      grade3: 9,
      grade4: 9
    });
    expect(result2.id).toBeTruthy();
    expect(result2.average).toEqual(8.5);
    expect(result2.situation).toEqual('approved');

    const result3 = await sut({
      studentId: 1,
      grade1: 4,
      grade2: 3,
      grade3: 3,
      grade4: 4
    });
    expect(result3.id).toBeTruthy();
    expect(result3.average).toEqual(3.5);
    expect(result3.situation).toEqual('disapproved');
  });
});