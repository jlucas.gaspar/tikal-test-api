import { resetDatabase } from './mocks';
import { pingInternal as sut } from '../core/pingInternal';

describe('grade service :: pingInternal', () => {
  beforeEach(() => resetDatabase());

  it('should return the ping message correctly', async () => {
    const result = await sut();
    expect(result).toBeTruthy();
  });
});