import { resetDatabase } from './mocks';
import { create } from '../core/create';
import { getByStudentId as sut } from '../core/getByStudentId';

describe('grade service :: getByStudentId', () => {
  beforeEach(() => resetDatabase());

  it('should get a grade by studentId correctly', async () => {
    await create({
      grade1: 10,
      grade2: 10,
      grade3: 10,
      grade4: 10,
      studentId: 1
    });
    const foundData = await sut({ studentId: 1 });
    expect(foundData?.average).toEqual(10);
  });
});