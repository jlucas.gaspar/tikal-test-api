import 'dotenv/config';
import { Knex } from 'knex';

export const TABLE_NAME = 'grades';

const knexfile: Knex.Config = {
  client: 'pg',
  connection: process.env.DB_URL
}

export default knexfile;