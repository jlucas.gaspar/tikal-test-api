import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return await knex.schema.createTable('grades', table => {
    table.increments('id').primary();

    table.bigInteger('studentId')
      .unsigned()
      .index()
      .references('id')
      .inTable('users')
      .notNullable();
    table.unique(['studentId']);

    table.float('grade1').notNullable();
    table.float('grade2').notNullable();
    table.float('grade3').notNullable();
    table.float('grade4').notNullable();
    table.float('average').notNullable();
    table.enum('situation', ['approved', 'disapproved', 'recovery']).notNullable();

    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').nullable();
    table.timestamp('deletedAt').nullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return await knex.schema.dropTable('grades');
}