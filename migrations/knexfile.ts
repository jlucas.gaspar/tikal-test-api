import dotenv from 'dotenv';

dotenv.config();

module.exports = {
  client: 'pg',
  connection: process.env.DB_URL,
  migrations: {
    tableName: 'migrations',
    directory: './src'
  }
}